#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_Waiting_List_Waiting_ListUITestsVersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_Waiting_List_Waiting_ListUITestsVersionString[];


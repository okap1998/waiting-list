//
//  Local.swift
//  WaitingList
//
//  Created by Vincenzo De Rosa on 04/03/2020.
//  Copyright © 2020 Vincenzo De Rosa. All rights reserved.
//
import SwiftUI
import CoreLocation

struct Local: Identifiable {
    var id: UUID = UUID()
    var type: String
    var name: String
    var address: String
    var image: String
    var coordinate = CLLocation()
    
    init(type: String, name: String, address: String, image: String, coordinate: CLLocation) {
        self.type = type
        self.name = name
        self.address = address
        self.image = image
        self.coordinate = coordinate
    }
}

var LocalList: [Local] = [
    Local(type: "Pizzeria", name: "Pignalosa", address: "Via Roma", image: "", coordinate: CLLocation(latitude: 40.836470, longitude: 14.306541)),
    Local(type: "Bar", name: "Umberto", address: "Via Roma", image: "", coordinate: CLLocation(latitude: 40.836470, longitude: 14.306541)),
    Local(type: "Pub", name: "Victory", address: "Via Roma", image: "", coordinate: CLLocation(latitude: 40.836470, longitude: 14.306541))
]

struct LocalItem: View {
    @State private var showLocalModal: Bool = false
    
    var type: String
    var name: String
    var address: String
    var image: String
    var coordinate = CLLocation()
    
    //    @ObservedObject var locationManager = LocationManager()
    
    //    var userLatitude: String {
    //        return "\(locationManager.lastLocation?.coordinate.latitude ?? 0)"
    //    }
    //
    //    var userLongitude: Double {
    //        return (locationManager.lastLocation?.coordinate.longitude ?? 0)
    //    }
    
    
    var body: some View {
        Button(action: {self.showLocalModal = true}) {
            HStack {
                Image(self.image)
                    .frame(width: 60.0, height: 50.0)
                    .border(Color(UIColor.label), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                    .cornerRadius(70)
                VStack{
                    Text(self.type)
                        .font(.headline)
//                        .foregroundColor(Color.black)
                    Text(self.name)
                        .font(.title)
//                        .foregroundColor(Color.black)
                    Text(self.address)
                        .font(.subheadline)
//                        .foregroundColor(Color.black)
                }
                Spacer()
                VStack{
                    Text("22m")
//                        .foregroundColor(Color.black)
                    Image("")
                        .frame(width: 20.0, height: 20.0)
                }
            }
            .padding(1.0)
        }
        .sheet(isPresented: self.$showLocalModal){
            LocalModalView() }
                .padding(5.0)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(UIColor.label), lineWidth: 1)
                        .shadow(color: Color(UIColor.label), radius: 2, x: 1, y: 1)
            )
        }
        
        init(type: String, name: String, address: String, image: String) {
            self.type = type
            self.name = name
            self.address = address
            self.image = image
        }
        
        init(local: Local) {
            self.type = local.type
            self.name = local.name
            self.address = local.address
            self.coordinate = local.coordinate
            self.image = local.image
        }
        
    }
    

struct LocalModalView: View {
    var body: some View {
        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
            Text(/*@START_MENU_TOKEN@*/"Button"/*@END_MENU_TOKEN@*/)
        }
    }
}

//struct Local_Previews: PreviewProvider {
//    static var previews: some View {
//        CustomButton()
//    }
//}

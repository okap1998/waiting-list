//
//  ContentView.swift
//  Waiting List
//
//  Created by Pasquale Di Donna on 19/02/2020.
//  Copyright © 2020 Pasquale Di Donna. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var isOwner = false
    @EnvironmentObject var session: SessionStore
    
    var body: some View {
        return Group {
            if isOwner {
                OwnerView()
            }
            else {
                NearbyView(isOwner: $isOwner)
            }
        }
    }
}

struct NearbyView: View {
    @Binding var isOwner: Bool
    
    @State private var showModal: Bool = false
    @State private var showModal2: Bool = false
    @EnvironmentObject var session: SessionStore
    func getUser(){
        session.listen()
    }
    var body: some View {
        NavigationView{
            VStack {
                List {
                    ForEach(LocalList) { local in
                        LocalItem(local: local)
                    }
                }
                    
                .navigationBarTitle("Nearby")
                .navigationBarItems(trailing:
                    Button(action: {
                        self.showModal = true
                    }) {
                        Image(systemName: "person.fill")
                            .accentColor(Color(UIColor.label))
                            .imageScale(.large)
                    }
                    .sheet(isPresented: self.$showModal){
                        Group{
                            if(self.session.session != nil){
                                ModalProfile(isOwner: self.$isOwner)
                                    .environmentObject(self.session)
                            }else{
                                ModalSignIn()
                                    .environmentObject(self.session)
                            }
                        }.onAppear(perform: self.getUser)
                    }
                )
                Button(action: {self.showModal2 = true}){
                    HStack{
                        Text("I'm a new user.")
                            .fontWeight(.light)
                            .foregroundColor(Color.gray)
                        
                        Text("Create an account")
                            .fontWeight(.semibold)
                            .foregroundColor(Color.green)
                        .saturation(1.8)
                    }
                }
                    
                .sheet(isPresented: self.$showModal2){
                    ModalSignUp()
                        .environmentObject(self.session)
                }
                
            }
        }
    }
}

struct ModalSignIn: View {
    
    @State private var showModal: Bool = false
    @State private var email: String = ""
    @State private var password: String = ""
    @State var error: String = ""
    @EnvironmentObject var session: SessionStore
    
    func signIn(){
        session.signIn(email: email, password: password) { (result,error) in
            if let error = error {
                self.error = error.localizedDescription
            }else{
                self.email = ""
                self.password = ""
            }
        }
    }
 
    var body: some View {
        
        VStack() {
            
            Spacer()
            VStack {
                Text("Welcome back")
                .font(.system(size:32,weight:.heavy))
                Text("Sign in to your account")
                .font(.system(size:14,weight:.regular))
            }  .padding()
            
            TextField("E-mail",text:self.$email)
                .padding()
                .background(Color.gray)
                .cornerRadius(20)
            SecureField("Password",text:self.$password)
                .padding()
                .background(Color.gray)
                .cornerRadius(20)
            
            Button(action: signIn){
                Text("Sign In")
                    
                    .font(.headline)
                    .foregroundColor(.green)
                    .padding()
                    .cornerRadius(20)
//                    .background(Color.green)
                    
            }
            .background(Capsule().stroke(Color.green,lineWidth: 2))
            .padding()
                .saturation(1.8)
            
            if (error != ""){
                Text(error)
                    .font(.system(size: 14, weight:.semibold))
                    .foregroundColor(.red)
                    .padding()
            }
            Spacer()
           
        }
        .padding()
    }
    
}


struct ModalProfile:View{
    @EnvironmentObject var session: SessionStore
    @Binding var isOwner: Bool
    var body: some View {
        
        VStack {
            Toggle(isOn: $isOwner) {
                Text("isOwner")
            }
            Button(action: session.signOut) {
                Text("SignOut")
            }
            
        }
    }
}
struct ModalSignUp: View{
    @State private var name: String = ""
    @State private var email: String = ""
    @State private var surname: String = ""
    @State private var password: String = ""
    @State var error: String = ""
    @EnvironmentObject var session: SessionStore
    
    func signUp(){
        session.signUp(email: email, password: password) {(result,error) in
            if let error = error {
                self.error = error.localizedDescription
            } else {
                self.email = ""
                self.password = ""
                self.name = ""
                self.surname = ""
                self.session.signOut()
            }
        }
    }
    var body: some View{
        
        VStack(){
            Spacer()
            Text("Create Account")
                .font(.system(size:32,weight:.heavy))
            
            
            
            VStack(){
                TextField("Name",text:self.$name)
                .padding()
                .background(Color.gray)
                .cornerRadius(20)
                TextField("Surname",text:self.$surname)
                    .padding()
                    .background(Color.gray)
                    .cornerRadius(20)
                
                TextField("E-mail",text:self.$email)
                    .padding()
                    .background(Color.gray)
                    .cornerRadius(20)
                
                SecureField("Password",text:self.$password)
                    .padding()
                    .background(Color.gray)
                    .cornerRadius(20)
                
                Button(action:signUp){
                Text("Submit")
                    .font(.headline)
                    .foregroundColor(.green)
                    .padding()
                    .cornerRadius(20)
                }
                    .background(Capsule().stroke(Color.green,lineWidth: 2))
                    .saturation(1.8)
                .padding()
                
            }
            .padding()
            
            if (error != ""){
                Text(error)
                    .font(.system(size: 14, weight:.semibold))
                    .foregroundColor(.red)
                    .padding()
            }
            Spacer()
            
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(SessionStore())
    }
}

//
//  SwiftUIView.swift
//  WaitingList
//
//  Created by Vincenzo De Rosa on 04/03/2020.
//  Copyright © 2020 Vincenzo De Rosa. All rights reserved.
//

import SwiftUI

struct Utente: Identifiable {
    var id:UUID = UUID()
    var name: String
}

struct ListElement: View {
    var name: String
    var body: some View {
        HStack {
            Button(action: {} ) {
                Text(name)
                    .padding(.leading)
            }
        }
    }
    
    init(name: String) {
        self.name = name
    }
    init(user: Utente) {
        self.name = user.name
    }
}

//ff

struct OwnerView: View {
    @State private var showModal: Bool = false
    @State var myList: [Utente] = [
        Utente(name: "1"),
        Utente(name: "2"),
        Utente(name: "3"),
        Utente(name: "4")
    ]
    
    var body: some View {
        NavigationView {
            List {
                ForEach(myList) { user in ListElement(user: user)}
                    .onDelete(perform: self.deleteRow)
            }
            .navigationBarTitle("List")
            .navigationBarItems(trailing:
                Button(action: {
                    self.showModal = true
                }) {
                    Image(systemName: "plus")
                }
                .sheet(isPresented: self.$showModal){
                    AddModal(isPresented: self.$showModal, didAddElement: { user in
                        self.myList.append(user)
                    }
                    )
                }
            )
        }
    }
    
    //    private func addRow(nome: String) {
    //        self.myList.append(Utente(name: nome))
    //    }
    
    private func deleteRow(at indexSet: IndexSet) {
        
        //        delete from database
        self.myList.remove(atOffsets: indexSet)
    }
}

struct AddModal: View {
    @Binding var isPresented: Bool
    
    @State var name: String = ""
    @State var number: Int = 1
    
    var didAddElement: (Utente) -> ()
    
    var body: some View {
        VStack {
            Text("Add People")
                .font(.title)
                .bold()
                .padding()
            HStack {
                Text("Name:")
                TextField("Enter name",text:self.$name)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            }
            Stepper(value: $number, in: 1...10) {
                Text("Table for: \(number)")
            }
            Button(action: {
                self.isPresented = false
                self.didAddElement(.init(name: self.name))
            }) {
                Text("Add")
                    .foregroundColor(.white)
                    .padding(.all)
                    .background(Color.green)
                
            }
            Button(action: {
                self.isPresented = false
            }) {
                Text("Cancel")
                    .foregroundColor(.white)
                    .padding(.all)
                    .background(Color.red)
            }
            .padding()
            Spacer()
        }
            
        .padding()
    }
    
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        OwnerView()
    }
}

